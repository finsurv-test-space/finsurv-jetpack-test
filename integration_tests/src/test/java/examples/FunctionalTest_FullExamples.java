package examples;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.IntegrationTestProperties;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions.getFileContent;

public class FunctionalTest_FullExamples {
    private static final Logger logger = LoggerFactory.getLogger(FunctionalTest_FullExamples.class);
    public static String channelName = "sbZA";
    public static String localArtefactDirectory = "../producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "";
    public static String reportDataStoreUsername = "";
    public static String reportDataStorePassword = "";
    public static String finsurvReportTemplateDirectory = "../Templates";
    public static String rootFinsurvReportTemplate = "RootTemplate.ftl";
    public static FinsurvLocal finsurv = null;
    public static long artefactsTTL = 60*1000;

    @Before
    public void setup() throws Exception{
        //Get this properties out of the config.properties folder
        Properties properties = IntegrationTestProperties.getInstance();
        reportDataStoreInstanceUrl = properties.getProperty("uriBase");
        reportDataStorePassword = properties.getProperty("password");
        reportDataStoreUsername = properties.getProperty("username");
        finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
        finsurv.addChannel(channelName);

    }

    /**
     * This class shows the whole process
     * 1) Data is retrieved from file
     * 2) Bulk evaluation of the data
     * 3) Bulk report generation from evaluated data
     * 4) Bulk validation from reports generated in previous step
     * 5) Bulk submit data to RDS
     *
     * Additional logic has been added at the bottom of this class
     * a) Valid data has been added to a queue - this transactions/reports are eligible for payment release
     * b) Invalid data has been added to a separate queue - this can be forwarded to an exception handling process
     *
     * @throws Exception
     */
    @Test
    public void bulkReportExample() throws Exception {
        //This start date is for logging purposes - see how long it took to go through the whole process
        LocalDateTime start = LocalDateTime.now();

        bootstrap();

        LocalDateTime startTime = LocalDateTime.now();

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new HashMap<String, Map<String, Object>>();

        //We simulate a continuous inflow of reports
            //-----------------------------------------------------------------------------------
            //  GENERATION OF EXAMPLE DATA (REPORTS)
            //-----------------------------------------------------------------------------------
            System.out.println("Starting the process to generate data");
            data.clear();

            String fileContentThatMakesExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalBulkJSON_001.json");
            String fileContentWithNoExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalBothSidesReportable.json");

            String[] content = {fileContentWithNoExternalCall,fileContentThatMakesExternalCall};

            long reportCount = Math.round((Math.random() * 10)+1);

            for (int i = 0; i < reportCount; i++) {
                //Select random scenario - one that makes external call and one that is just a normal validation without external calls
                int randomFile = (int) Math.round(Math.random() * 1);
                //Generate unique record based on TRN Reference
                Map<String, Object> eval = JsonUtils.jsonStrToMap(content[randomFile].replaceAll("SomeReference_0001", "SomeReference_" + String.format("%06d", i)));
                data.put((String) eval.get("TrnReference"), eval);
            }

            //-----------------------------------------------------------------------------------
            //  BULK EVALUATION, REPORT GENERATION, VALIDATION AND SUBMISSION TO RDS
            //-----------------------------------------------------------------------------------

            //Perform bulk evaluation
            finsurv.bulkEvaluateTransactionData(channelName, data, false, false);

            //Perform bulk report generation
            finsurv.bulkGenerateDrCrBopReport(data);

            //Perform bulk report validation
            finsurv.bulkValidateDrCrTransactionData(channelName, data);

            //Example to print out validation results
//          for(Map.Entry<String, Map<String,Object>> entry : data.entrySet()){
//            List<ResultEntry> validationResults = (List<ResultEntry>) entry.getValue().get("ValidationResult");
//            for (ResultEntry resultEntry : validationResults) {
//                if ("ValueDate".equalsIgnoreCase(resultEntry.getName()) && "Error".equalsIgnoreCase(resultEntry.getType().name())) {
//                    System.out.println("VALIDATION "+(resultEntry.getType().name())+": " + resultEntry.getCode() + " - " + resultEntry.getName() + " :: " + resultEntry.getMessage());
//                }
//            }
//          }
            //Perform bulk report submission to RDS (regardless of validation status)
            finsurv.bulkSubmitDrCrBopReport(channelName, data, null);      //This method creates a list from the reports and send it as a single object - recommended function to use.

            //-----------------------------------------------------------------------------------
            //  EXAMPLE LOGIC FOR INTEGRATION BY CLIENTS:
            //-----------------------------------------------------------------------------------

            //determine which transactions/reports:
            // 1. need to be forwarded to an exception queue (failed validations)
            // 2. can be released (passed validations)

//          HashMap<String, Map<String, Object>> exceptionQueue = new HashMap<String, Map<String, Object>>();
//          HashMap<String, Map<String, Object>> releaseQueue = new HashMap<String, Map<String, Object>>();
//          String vrKey = "ValidationResult";
//          for (Map.Entry<String, Map<String, Object>> entry : data.entrySet()){
//              Map<String, Object> trnData = entry.getValue();
//                  if (trnData != null){
//                      if (trnData.containsKey(vrKey) && trnData.get(vrKey) instanceof List){
//                          List<ResultEntry> validationResult = (List<ResultEntry>)trnData.get(vrKey);
//                          //count errors and warnings...
//
//                          if (finsurv.isValidationSuccessful(validationResult)) { //The transaction/report passed validation
//                              releaseQueue.put(entry.getKey(), trnData);
//                              logger.info("Transaction "+entry.getKey()+" is eligible for release (Successful Validation)");
//                          } else {//The transaction/report failed validation
//                              exceptionQueue.put(entry.getKey(), trnData);
//                              logger.info("Transaction "+entry.getKey()+" must be forwarded to an exception workflow/queue due to errors (Failed Validation)");
//                          }
//                      }
//                  }
//          }

            data.clear();
            Thread.sleep(1000);
            logger.info("\n\n####################################################\n\n");
            logger.info("\n\n\nFinished processing.");
    }


    @Test
    public void bulkFullStressTest() throws Exception{
        //TODO: Create threadpool and each thread needs a worker that will execute bulkReportExample() above and then sleep for a period (random seconds 5-120)
        int threadPoolSize = 10;
        ExecutorService threadpool = Executors.newFixedThreadPool(threadPoolSize);

        //TODO: what do we do to make it stop?   random number (1-10) of iterations per thread worker?
        ArrayList<WorkerStressTest> workers = new ArrayList<WorkerStressTest>();
        for(int workerCount = 0; workerCount < 10; workerCount++){
            workers.add(new WorkerStressTest((int) Math.round(Math.random() * 3 + 4)));
        }

        boolean workersAlive = true;

        int iterationCount = 0;
        while (workersAlive){
            workersAlive = false;
            for (WorkerStressTest worker: workers){
                System.out.println("\n\nRunning worker "+workers.indexOf(worker) + " Complete: " +worker.complete );
                if (worker.complete && worker.iterations > 0){
                    System.out.println("\nWorker "+workers.indexOf(worker) + " is at iteration "+worker.iterations);
                    worker.iterations--;
                    threadpool.execute(worker);
                    workersAlive = true;
                } else if (!worker.complete){
                    workersAlive = true;
                }
            }
            if (workersAlive) {
                System.out.println("Sleeping to give threads time to complete");
                Thread.sleep(2000);
            }
            iterationCount++;
            if (iterationCount % 10 == 0) {
                finsurv.refreshProcessingThreadPool();
                finsurv.refreshSubmissionThreadPool();
            }
        }

        threadpool.shutdown();
        threadpool.awaitTermination(5, TimeUnit.MINUTES);
    }


    public class WorkerStressTest implements Runnable{
        int iterations = 0;
        boolean complete = true;

        public WorkerStressTest(int iterations){
            this.iterations = iterations;
        }

        @Override
        public void run() {
            this.complete = false;
            try {
                Thread.sleep(Math.round(Math.random() * 10000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                bulkReportExample();
                this.complete = true;
            }catch (Exception e){
                System.out.println("Error");
            }
        }
    }

    /**
     * This bootstrap code is basic setup that we need to specify in order to do evaluation, generation of the bop report, validation of the bop report and submission to RDS.
     * Setup the finsurv local instance with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
     * Set the thread timeout period and cores multipliers for the processing and submission thread pools.
     * Wait until the artefacts are ready for processing before we move on and use them for evaluation etc.
     */
    public synchronized void bootstrap(){
        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        if (finsurv == null) {
            System.out.println("Bootstrapping finsurv");
            //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
            finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);

        /*
            Set the processing thread pool timeout period and cores multiplier value
                - this value should not exceed the number of cores available - the system will become unresponsive
         */
            finsurv.setProcessingThreadPoolCoresMultiplier(1);
            finsurv.setProcessingThreadPoolShutdownAwaitMinutes(5);
        /*
            Set the submission thread pool timeout period and multiplier value
                - this multiplier can exceed the number of available cores - this is not a CPU intensive thread pool
         */
            finsurv.setSubmissionThreadPoolCoresMultiplier(20);
            finsurv.setSubmissionThreadPoolShutdownAwaitMinutes(5);

            //Add a specific channel to use
            finsurv.addChannel(channelName);

            // Refresh the local artefact cache from RDS
            finsurv.waitTillReadyForProcessing();
            System.out.println("Finsurv local ready for processing");
        }
    }

    @Test
    public void simpleSingleReportExample() throws Exception {
        //This start date is for logging purposes - see how long it took to go through the whole process
        LocalDateTime start = LocalDateTime.now();
        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentails, report directory and root template name.
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
        //Add a specific channel to use
        finsurv.addChannel(channelName);

        // Refresh the local artefact cache from RDS
        finsurv.waitTillReadyForProcessing();

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();
        String fileContentWithNoExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalBulkJSON_002.json");

        Map<String, Object> eval = JsonUtils.jsonStrToMap(fileContentWithNoExternalCall);
        String trnReference = (String) eval.get("TrnReference");
        data.put(trnReference, eval);
        //-----------------------------------------------------------------------------------
        //  BULK EVALUATION, REPORT GENERATION, VALIDATION AND SUBMISSION TO RDS
        //-----------------------------------------------------------------------------------

        for (Map.Entry<String, Map<String, Object>> entry : data.entrySet()) {
            Map<String, Object> trnData = entry.getValue();
            if (trnData != null) {
                finsurv.evaluateBopReport(channelName, trnData, false, false, trnReference);
                finsurv.composeDrCrTemplate(trnData);
                finsurv.validateDrCrBopReport(channelName, entry.getKey(), (Map<String, String>) trnData.get("Composed"), trnData);
                finsurv.submitSingleDrCrReportToRDS(channelName, (Map<String, String>) trnData.get("Composed"));
            }
        }
        logger.info("\n\n####################################################\n\n");
        logger.info("\n\n\nFinished processing.");
    }

    @Test
    public void singleDrCrReportExample() throws Exception {
        //This start date is for logging purposes - see how long it took to go through the whole process
        LocalDateTime start = LocalDateTime.now();
        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentails, report directory and root template name.
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
        //Add a specific channel to use
        finsurv.addChannel(channelName);

        // Refresh the local artefact cache from RDS
        finsurv.waitTillReadyForProcessing();

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();

        String fileContentWithNoExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalBothSidesReportable.json");
//        String fileContentWithNoExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalDrSideReportable.json");

        Map<String, Object> eval = JsonUtils.jsonStrToMap(fileContentWithNoExternalCall);
        String trnReference = (String) eval.get("TrnReference");
        data.put(trnReference, eval);
        //-----------------------------------------------------------------------------------
        //  BULK EVALUATION, REPORT GENERATION, VALIDATION AND SUBMISSION TO RDS
        //-----------------------------------------------------------------------------------

        for (Map.Entry<String, Map<String, Object>> entry : data.entrySet()) {
            Map<String, Object> trnData = entry.getValue();
            if (trnData != null) {
                finsurv.evaluateBopReport(channelName, trnData, false, false, trnReference);
                finsurv.composeDrCrTemplate(trnData);
                finsurv.validateDrCrBopReport(channelName, entry.getKey(), (Map<String, String>) trnData.get("Composed"), trnData);
                finsurv.submitSingleDrCrReportToRDS(channelName, (Map<String, String>) trnData.get("Composed"));
            }
        }
        logger.info("\n\n####################################################\n\n");
        logger.info("\n\n\nFinished processing.");
    }



}
