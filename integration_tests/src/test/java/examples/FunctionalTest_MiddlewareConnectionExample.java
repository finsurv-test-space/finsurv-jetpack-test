package examples;

import org.junit.Before;
import org.junit.Test;
import utils.IntegrationTestProperties;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class FunctionalTest_MiddlewareConnectionExample {
    public static String channelName = "sbZA";
    public static String localArtefactDirectory = "../producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "";
    public static String reportDataStoreUsername = "";
    public static String reportDataStorePassword = "";
    public static String finsurvReportTemplateDirectory = null;
    public static String rootFinsurvReportTemplate = null;
    public static long artefactsTTL = 60*1000;
    FinsurvLocal finsurv = null;

    @Before
    public void setup() throws Exception{
        //Get this properties out of the config.properties folder
        Properties properties = IntegrationTestProperties.getInstance();
        reportDataStoreInstanceUrl = properties.getProperty("uriBase");
        reportDataStorePassword = properties.getProperty("password");
        reportDataStoreUsername = properties.getProperty("username");
        finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
        finsurv.addChannel(channelName);
    }

    @Test
    public void testSettingUpConnectionConfig() {
        String config = CommonHelperFunctions.getFileContent("../sample/ReportDataStoreApiConfig.json");
        String data = CommonHelperFunctions.getFileContent("../sample/valid_sarb_transaction.json");
        try {
            Map<String, Object> mappedConfig = (JsonUtils.jsonStrToMap(config));

            String urlTemplate = null;
            String httpRequestMethod = null;
            String sslContext = null;
            String payloadTemplate = null;
            Map<String, Object> keyStoreTemplate = new HashMap<>();
            Map<String, Object> trustStoreTemplate = new HashMap<>();
            Map<String, String> headerTemplates = new HashMap<>();

            for (Map.Entry<String, Object> entry : mappedConfig.entrySet()) {
                Map<String, Object> individualConfigItems = (Map<String, Object>) entry.getValue();
                for (Map.Entry<String, Object> individualConfig : individualConfigItems.entrySet()) {
                    if ("urlTemplate".equalsIgnoreCase(individualConfig.getKey())) {
                        urlTemplate = (String) individualConfig.getValue();
                    }
                    if ("keyStoreTemplate".equalsIgnoreCase(individualConfig.getKey())) {
                        Map<String, Object> keystoreFields = (Map<String, Object>) individualConfig.getValue();
                        for (Map.Entry<String, Object> keystoreField : keystoreFields.entrySet()) {
                            keyStoreTemplate.put(keystoreField.getKey(), keystoreField.getValue());
                        }
                    }
                    if ("trustStoreTemplate".equalsIgnoreCase(individualConfig.getKey())) {
                        Map<String, Object> trustStoreFields = (Map<String, Object>) individualConfig.getValue();
                        for (Map.Entry<String, Object> truststoreField : trustStoreFields.entrySet()) {
                            trustStoreTemplate.put(truststoreField.getKey(), truststoreField.getValue());
                        }
                    }
                    if ("headerTemplates".equalsIgnoreCase(individualConfig.getKey())) {
                        Map<String, Object> headerTemplateFields = (Map<String, Object>) individualConfig.getValue();
                        for (Map.Entry<String, Object> headerTemplateField : headerTemplateFields.entrySet()) {
                            headerTemplates.put(headerTemplateField.getKey(), (String) headerTemplateField.getValue());
                        }
                    }
                    if ("httpRequestMethod".equalsIgnoreCase(individualConfig.getKey())) {
                        httpRequestMethod = (String) individualConfig.getValue();
                    }
                    if ("sslContext".equalsIgnoreCase(individualConfig.getKey())) {
                        sslContext = (String) individualConfig.getValue();
                    }
                }
            }
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("channelName", channelName);

            //TODO: FIX THIS TEST CASE - I SUSPECT IT IS BECAUSE OF KEYSTORE/TRUSTSTORE ISSUES
            //finsurv.getHttpsConnection(httpRequestMethod,urlTemplate, payloadTemplate, headerTemplates, params, sslContext, keyStoreTemplate, trustStoreTemplate, data);

            System.out.println("nope");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
