package examples;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.IntegrationTestProperties;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;
import za.co.synthesis.finsurvlocal.validation.CustomRestCallValidate;
import za.co.synthesis.finsurvlocal.validation.types.Endpoint;
import za.co.synthesis.finsurvlocal.validation.types.EndpointParameter;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static za.co.synthesis.finsurvlocal.validation.Validation.*;


/**
 * NOTE: All relevant functions and logic for using the rules engine can be found in the za.co.synthesis.example.POC class.
 * Important functions to look at are as follows:
 * - getPotentialDocuments
 * - getRequiredDocuments
 * - doEvaluationExample
 * - doValidationExample
 * - doRevisedValidationExample
 * <p>
 * ...all other logic and functions are fluff to make the POC/example code simple to implement and run as test cases etc.
 */
public class FunctionalTest_ValidationTests {

    private static final Logger logger = LoggerFactory.getLogger(FunctionalTest_ValidationTests.class);
    public static String channelName = "sbNA";
    public static String localArtefactDirectory = "../producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "";
    public static String reportDataStoreUsername = "";
    public static String reportDataStorePassword = "";
    public static String finsurvReportTemplateDirectory = "../Templates";
    public static String rootFinsurvReportTemplate = "RootTemplate.ftl";
    public static long artefactsTTL = 60*1000;
    FinsurvLocal finsurv = null;

    @Before
    public void setup() throws Exception{
        //Get this properties out of the config.properties folder
        Properties properties = IntegrationTestProperties.getInstance();
        reportDataStoreInstanceUrl = properties.getProperty("uriBase");
        reportDataStorePassword = properties.getProperty("password");
        reportDataStoreUsername = properties.getProperty("username");
        finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
        finsurv.addChannel(channelName);
    }


    @Test
    public void testExternalValidationCall() {
        try {
            List<Endpoint> configs = getExternalValidationConfigList(channelName);
            for (Endpoint config : configs) {
                CustomRestCallValidate implementation = new CustomRestCallValidate(channelName, config, finsurv.getRdsConfig());
                List<String> paramValues = new ArrayList();
                for (EndpointParameter param : config.getEndpointParameterList()) {
                    //if (param
                    paramValues.add("SOMEVALUE");
                }
                CustomValidateResult result = implementation.call(config.getName(), paramValues.toArray());
                logger.info("Result :" +result.toString());
            }
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }

    /**
     * Use the older version of validation function on the validator - more onerous to implement.
     */
    @Test
    public void testSARBValidation() {
        //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
        try {
            Validator validator = getValidatorForPackage(channelName, finsurv.getRdsConfig());
            validSarbData = isValidationSuccessful(validateJsonBopData(validator, CommonHelperFunctions.getFileContent("../sample/ExampleBopData/ExampleFinsurvJSON.json")));
        } catch (Exception e) {
            validSarbData = false;
        }
    }

    /**
     * Use the older version of validation function on the validator - more onerous to implement.
     */
    @Test
    public void testPredef() {
        // * Refresh the local artefact cache from RDS
       // finsurv.updateAllChannelsArtefacts();
        boolean validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/TestNewPredef.json"), channelName,finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        System.out.println("Im done");
    }

    /**
     * Use the newer function to test with - much less onerous to integrate with...
     */
    @Test
    public void testSARBValidationWithNewFunction() {
        //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
//        try {
//            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON.json"), "sbZA");
//        } catch (Exception e) {
//            validSarbData = false;
//        }
        //assert(!validSarbData);

        validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/ExampleFinsurvJSON2.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(validSarbData);
    }

    @Test
    public void testSARBVaildationtion_LocalCurrencyIndividual_Resident_X_USD() {
         //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_RES-X-USD-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(!validSarbData);

        validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_RES-X-USD-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(validSarbData);
    }

    @Test
    public void testSARBVaildationtion_LocalCurrencyIndividual_Resident_X_ZAR() {
        //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_RES-X-ZAR-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(!validSarbData);

        validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_RES-X-ZAR-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(validSarbData);
    }


    @Test
    public void testSARBVaildationtion_LocalCurrencyIndividual_NonResident_X_USD() {
       //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_NRES-X-USD-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(!validSarbData);

        validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_NRES-X-USD-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(validSarbData);
    }

    @Test
    public void testSARBVaildationtion_LocalCurrencyIndividual_NonResident_X_ZAR() {
        //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_NRES-X-ZAR-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(!validSarbData);

        validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/LCY_IND_NRES-X-ZAR-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(validSarbData);
    }

    @Test
    public void testSARBVaildationtion_X_FCA_IND() {
        //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/X-FCA_IND-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(!validSarbData);

        validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/X-FCA_IND-Bop.json"), "sbZA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(validSarbData);
    }

    @Test
    public void testBONValidation() {
        //I expect these to fail as the JSON provided is incomplete.
        boolean validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/ExampleFinsurvJSON.json"), "sbNA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(!validSarbData);

        validSarbData = false;
        try {
            validSarbData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/ExampleFinsurvJSON2.json"), "sbNA",finsurv.getRdsConfig());
        } catch (Exception e) {
            validSarbData = false;
        }
        //assert(validSarbData);
    }


    @Test
    public void testRBMValidation() {
        //I expect these to fail as the JSON provided is incomplete.
        boolean validData = false;
        try {
            validData = isBopJsonValid(CommonHelperFunctions.getFileContent("../sample/ExampleBopData/ExampleFinsurvJSON_RBM.json"), "sbMW",finsurv.getRdsConfig());
        } catch (Exception e) {
            validData = false;
        }
    }


}
