package examples;

import org.junit.Before;
import org.junit.Test;
import utils.IntegrationTestProperties;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;

import java.util.Properties;


/**
 * This is an example of submission and retrieval of a single report to/from RDS
 */
public class FunctionalTests_SingleReportRDS {
    public static String channelName = "sbZA";
    public static String localArtefactDirectory = "../producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "";
    public static String reportDataStoreUsername = "";
    public static String reportDataStorePassword = "";
    public static String finsurvReportTemplateDirectory = null;
    public static String rootFinsurvReportTemplate = null;
    public static long artefactsTTL = 60*1000;
    FinsurvLocal finsurv = null;

    @Before
    public void setup() throws Exception{
        //Get this properties out of the config.properties folder
        Properties properties = IntegrationTestProperties.getInstance();
        reportDataStoreInstanceUrl = properties.getProperty("uriBase");
        reportDataStorePassword = properties.getProperty("password");
        reportDataStoreUsername = properties.getProperty("username");
        finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
        finsurv.addChannel(channelName);
    }

    @Test
    public void testSubmittingOfSingleReportToRDS() throws Exception {
        String data = CommonHelperFunctions.getFileContent("../sample/valid_sarb_transaction.json");
        finsurv.submitSingleReportToRDS(channelName,data);
    }

    @Test
    public void testFetchingOfSingleReportFromRDS(){
        finsurv.getSingleReportFromRDS(channelName,"TestRef12424");
    }

    @Test
    public void testReportHistoryFromRDS(){
        finsurv.getReportDiff(channelName,"TestRef12424");
    }
}
