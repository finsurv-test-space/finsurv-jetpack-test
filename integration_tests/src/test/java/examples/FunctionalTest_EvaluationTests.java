package examples;

import org.junit.Before;
import org.junit.Test;
import utils.IntegrationTestProperties;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.evaluation.types.EvalParams;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;
import za.co.synthesis.rule.core.IEvaluationDecision;
import za.co.synthesis.rule.core.impl.EvaluationScenarioDecision;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import static za.co.synthesis.finsurvlocal.evaluation.Evaluation.evaluateTransactionData;
import static za.co.synthesis.finsurvlocal.evaluation.Evaluation.getEvaluationParameters;
import static za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions.getFileContent;

public class FunctionalTest_EvaluationTests {
  public static String[] channelNames = {"sbZA","sbLS","sbMW","sbNA"};
  public static String localArtefactDirectory = "../producer/api/rules/";
  public static String reportDataStoreInstanceUrl = "";
  public static String reportDataStoreUsername = "";
  public static String reportDataStorePassword = "";
  public static String finsurvReportTemplateDirectory = "../Templates";
  public static String rootFinsurvReportTemplate = "RootTemplate.ftl";
  public static long artefactsTTL = 60*1000;
  FinsurvLocal finsurv = null;

  @Before
  public void setup() throws Exception{
    //Get this properties out of the config.properties folder
    Properties properties = IntegrationTestProperties.getInstance();
    reportDataStoreInstanceUrl = properties.getProperty("uriBase");
    reportDataStorePassword = properties.getProperty("password");
    reportDataStoreUsername = properties.getProperty("username");
    finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
    finsurv.addChannels(channelNames);

  }

  @Test
  public void testSARBEvaluation(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalJSON.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);

      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_001(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalJSON_001.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSBLSEvaluation_001(){
    String packageName = "sbLS";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/EE_LS_Eval_Failure.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_NonResident_X_ZAR(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_NRES-X-ZAR.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrency_USD_InsufficientData(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_NRES-X-USD_INSUFFICIENT_DATA.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
//      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_NonResident_X_USD(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_NRES-X-USD.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_Resident_X_ZAR(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_RES-X-ZAR.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_Resident_X_USD(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_RES-X-USD.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_Resident_LocalCurrencyIndividual_Resident(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_RES-LCY_IND_RES.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_Resident_LocalCurrencyIndividual_NonResident(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_RES-LCY_IND_NRES.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_NonResident_LocalCurrencyIndividual_Resident(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_NRES-LCY_IND_RES.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_LocalCurrencyIndividual_NonResident_LocalCurrencyIndividual_NonResident(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/LCY_IND_NRES-LCY_IND_NRES.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_X_LocalCurrencyIndividual_Resident(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/X-LCY_IND_RES.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_X_LocalCurrencyIndividual_NonResident(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/X-LCY_IND_NRES.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_X_CFC_ENTITY(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/X-CFC_ENT.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_CFC_ENTITY_LOCAL(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/CFC-LOCAL.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testSARBEvaluation_X_FCA_INDIVIDUAL(){
    String packageName = "sbZA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/X-FCA_IND.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testBONEvaluation(){
    String packageName = "sbNA";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalJSON.json");
      System.out.println("Scenario Input Data:\n"+fileContent);
      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void testRBMEvaluation(){
    String packageName = "sbMW";
    //I expect these to fail as the JSON provided is incomplete.
    EvaluationScenarioDecision evalDecisions;
    try {
      String fileContent = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalJSON.json");
      System.out.println("Scenario Input Data:\n"+fileContent);

      EvalParams params = getEvaluationParameters(fileContent);
      evalDecisions = (EvaluationScenarioDecision) evaluateTransactionData(packageName,params,true,true);
    } catch (Exception e){
      evalDecisions = null;
    }
    assert(evalDecisions != null);
    for (IEvaluationDecision decision : evalDecisions.getDecisions()){
      System.out.println("["+packageName+"] Eval Decision ("+decision.getReportingSide()+") : "+decision.getReportable()+", "+decision.getFlow());
    }
    System.out.println(" ------------------------------------------------------------------------");
    System.out.println("|                       END       OF      TEST                           |");
    System.out.println(" ------------------------------------------------------------------------");
  }

  @Test
  public void bulkEvaluate() throws Exception {
    //This start date is for logging purposes - see how long it took to go through the whole process
    LocalDateTime start = LocalDateTime.now();
    String channelName = "sbZA";
    //-----------------------------------------------------------------------------------
    //  BOOTSTRAP CODE
    //-----------------------------------------------------------------------------------

//    //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
//    FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
//    //Add a specific channel to use
//    finsurv.addChannel(channelName);

    // * Refresh the local artefact cache from RDS
    finsurv.updateAllChannelsArtefacts();

    //This is just to generate unique records
    Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();
    String fileContentThatMakesExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalBulkJSON_001.json");
    String fileContentWithNoExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalBothSidesReportable.json");
//        String fileContentWithNoExternalCall = getFileContent("../sample/ExampleEvalData/ExampleFinsurvEvalBulkJSON_002.json");
    String[] content = {fileContentThatMakesExternalCall,fileContentWithNoExternalCall};
    for (int i = 0; i < 10; i++) {
      //Select random scenario - one that makes external call and one that is just a normal validation without external calls
      int randomFile = (int) Math.round(Math.random()*1);
      //Generate unique record based on TRN Reference
      Map<String, Object> eval = JsonUtils.jsonStrToMap(content[randomFile].replaceAll("SomeReference_0001", "SomeReference_" + String.format("%06d", i)));
      data.put((String) eval.get("TrnReference"), eval);
    }

    //-----------------------------------------------------------------------------------
    //  BULK EVALUATION, REPORT GENERATION, VALIDATION AND SUBMISSION TO RDS
    //-----------------------------------------------------------------------------------

    //Perform bulk evaluation
    finsurv.bulkEvaluateTransactionData(channelName, data, false, false);
    System.out.println("Example complete.");
  }


}
