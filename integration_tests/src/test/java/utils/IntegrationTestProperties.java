package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class IntegrationTestProperties extends Properties {

    private final static String PROPERTY_FILE_NAME_ENVIRONMENT_VARIABLE = "INTEGRATION_TEST_PROPERTY_FILE";
    private final static String PROPERTY_FILE_NAME_DEFAULT = "config.properties";

    private static IntegrationTestProperties instance;
    private String propertyFileName;

    private IntegrationTestProperties(String propertyFileName) throws IOException {
        this.propertyFileName = propertyFileName;
        initializeProperties();
    }

    public static IntegrationTestProperties getInstance() throws IOException {
        if (System.getenv(PROPERTY_FILE_NAME_ENVIRONMENT_VARIABLE) == null)
            return getInstance(PROPERTY_FILE_NAME_DEFAULT);
        else
            return getInstance(System.getenv(PROPERTY_FILE_NAME_ENVIRONMENT_VARIABLE));
    }

    public static IntegrationTestProperties getInstance(String propertyFile) throws IOException {
        if (instance == null)
            instance = new IntegrationTestProperties(propertyFile);
        return instance;
    }

    public String getPropertyFileName() {
        return propertyFileName;
    }

    private void initializeProperties() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertyFileName);
        this.load(inputStream);
        if (inputStream != null)
            inputStream.close();
        else
            throw new FileNotFoundException("Could not find property file " + propertyFileName);
    }
}
