package artefact;

import org.junit.Before;
import org.junit.Test;
import utils.IntegrationTestProperties;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import java.util.Properties;

public class FunctionalTest_ArtefactUpdateTest {
    public static String[] channelNames = {"sbZA","sbLS","sbMW","sbNA"};
    public static String localArtefactDirectory = "../producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "";
    public static String reportDataStoreUsername = "";
    public static String reportDataStorePassword = "";
    public static String finsurvReportTemplateDirectory = null;
    public static String rootFinsurvReportTemplate = null;
    public static long artefactsTTL = 60*1000;

    @Before
    public void retrieveCredentials() throws Exception{
        //Get this properties out of the config.properties folder
        Properties properties = IntegrationTestProperties.getInstance();
        reportDataStoreInstanceUrl = properties.getProperty("uriBase");
        reportDataStorePassword = properties.getProperty("password");
        reportDataStoreUsername = properties.getProperty("username");
    }

    @Test
    public void checkArtefactRetrieval(){
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate,artefactsTTL);
        finsurv.addChannels(channelNames);
        finsurv.waitTillReadyForProcessing();

        int cntr = 100;
        while(--cntr > 0 ){
            try {
                System.out.print(".");
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
