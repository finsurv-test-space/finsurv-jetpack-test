package util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ScrubUtils {
    public static String globalSeed = "Someglobalseedstring";
    public static String ALPHA_NUMERIC_LIST_LOWER_ALPHA = "abcdefghijklmnopqrstuvwxyz";
    public static String ALPHA_NUMERIC_LIST_UPPER_ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static String ALPHA_NUMERIC_LIST_NUMERIC = "0123456789";
    public static String ALPHA_NUMERIC_LIST_LOWER_ALPHA_SHUFFLED = "";
    public static String ALPHA_NUMERIC_LIST_UPPER_ALPHA_SHUFFLED = "";
    public static String ALPHA_NUMERIC_LIST_NUMERIC_SHUFFLED = "";
    public static String COMMA_SUBS = "[COMMA]";
    public static String QUOTE_SUBS = "[QUOTE]";

    public static String GenSAID() {
        return GenSAID(0, true, true);
    }

    public static String GenSAID(String id) {
        int dob = 0;
        boolean male = true;
        boolean citizen = true;
        if (!"NULL".equalsIgnoreCase(id)) {
            try {
                dob = parseInt(id.substring(0, 6));
            } catch (Exception err) {
            }
            try {
                male = parseInt(id.substring(6, 7), 0) >= 5;
            } catch (Exception err) {
            }
            try {
                citizen = parseInt(id.substring(10, 11), 0) == 0;
            } catch (Exception err) {
            }
            return GenSAID(dob, male, citizen);
        }
        return "";
    }

    //if this sucks, then look here: http://knowles.co.za/generating-south-african-id-numbers/
    //there is a utility class in this package (IDNumberValidatorUtility) sourced from the above address to do validations and extractions etc.
    public static String GenSAID(long dob, boolean male, boolean citizen) {
        String idString;
        if (!((dob >= 101) && (dob <= 991231))) {
            dob = (Math.round(Math.random() * (99)) * 10000) +
                    ((Math.round(Math.random() * 11) + 1) * 100) +
                    ((Math.round(Math.random() * 27) + 1));
        }
        idString = "" + lpad(dob, 6, '0');
        idString += (male ? "5" : "3");
        idString += lpad((Math.round(Math.random() * (999))), 3, '0');
        idString += (citizen ? "0" : "1");
        idString += "8"; //This was an indicator for ethnicity/religion - no longer used, would either be 8 or 9 now.
        idString += SAIDChecksum(idString);
        return idString;

    }

    public static String GenVarDate(Calendar dt, Integer maxVarDays){
        int randDays = (maxVarDays != null && maxVarDays > 0) ? (int)Math.floor(((maxVarDays * Math.random()) * 2 ) - maxVarDays) : 0;
        return GenDate(dt,randDays);
    }

    public static String GenDate(Calendar dt, Integer offsetDays){
        if (offsetDays != 0) {
            dt.add(Calendar.DAY_OF_MONTH, offsetDays);
        }
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        return format1.format(dt.getTime());
    }

    private static Date SAIDDob(String idString) {
        //implement extraction of the DOB from SA ID...
        Integer currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int year = 0;
        try{
            year = parseInt(idString.substring(0,2));
            if ((year + 2000) >= currentYear) {
                year += 1900;
            } else {
                year += 2000;
            }
        } catch (Exception err){}

        String dateString = "" + year + "-" + idString.substring(2,4) + "-" + idString.substring(4,6);
        return StrToDate(dateString, "yyyy-MM-dd");
    }

    private static Date StrToDate(String dateString, String format){
        Date calDate = null;
        DateFormat df = new SimpleDateFormat(format);
        try {
            calDate = df.parse(dateString);
//            String newDateString = df.format(calDate);
//            System.out.println(newDateString);
        } catch (Exception e) {
        }
        return calDate;
    }

    private static Calendar StrToCal(String dateString, String format){ //format: "yyyy/MM/dd HH:mm:ss z "
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        try {
            cal.setTime(sdf.parse(dateString));
        } catch (Exception err){}
        return cal;
    }


    private static int SAIDChecksum(String idString) {
        int d = 0;
        try {
            int a = 0;
            for (int i = 0; i < 6; i++) {
                a += parseInt(idString.charAt(2 * i) + "");
            }
            int b = 0;
            for (int i = 0; i < 6; i++) {
                b = b * 10 + parseInt(idString.charAt(2 * i + 1) + "");
            }
            b *= 2;
            int c = 0;
            do {
                c += b % 10;
                b = b / 10;
            } while (b > 0);
            c += a;
            d = 10 - (c % 10);
            if (d == 10) d = 0;
        } catch (Exception err) {
            System.err.println("Error creating ID Checksum: " + err.getMessage());
        }
        return d;
    }

    public static int parseInt(String val) {
        return parseInt(val, 0);
    }

    public static int parseInt(String val, int def) {
        int r = def;
        try {
            r = Integer.parseInt(val);
        } catch (Exception err) {
            //r=def;
        }
        return r;
    }

    public static String lpad(String s, int len, char padchar) {
        return String.format("%1$" + padchar + len + "s", s);
    }

    public static String lpad(long s, int len, char padchar) {
        return String.format("%1$" + padchar + len + "d", s);
    }

    public static Random getStrRandSeed(String input) {
        input = input == null ? globalSeed : input;
        return new Random(input.hashCode());
    }

    /**
     * this function will not change the value of the content of the provided string - other than shuffling the order thereof
     */
    public static String SelfShuffleString(String input) {
        return SeededShuffleString(input, input);
    }

    public static String SeededShuffleString(String input, String seed) {
        String output = null;
        if (input != null) {
            output = "";
            Random random = getStrRandSeed(seed);
            String[] inputs = input.split(" ");
            for (String input1 : inputs) {
                char[] g = input1.toCharArray();
                for (int y = 0; y < g.length; y++) {
                    int k = random.nextInt(g.length);
                    char s = g[y];
                    g[y] = g[k];
                    g[k] = s;
                }
                output += (output.length() > 0 ? " " : "") + (new String(g));
            }
        }
        return output;
    }

    public static String GlobalSubstituteString(String input) {
        if (ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA_SHUFFLED.length() == 0) {
            ScrubUtils.ALPHA_NUMERIC_LIST_NUMERIC_SHUFFLED = SeededShuffleString(ScrubUtils.ALPHA_NUMERIC_LIST_NUMERIC, ScrubUtils.globalSeed);
            ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA_SHUFFLED = SeededShuffleString(ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA, ScrubUtils.ALPHA_NUMERIC_LIST_NUMERIC_SHUFFLED).toLowerCase();
            ScrubUtils.ALPHA_NUMERIC_LIST_UPPER_ALPHA_SHUFFLED = SeededShuffleString(ScrubUtils.ALPHA_NUMERIC_LIST_UPPER_ALPHA, ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA_SHUFFLED).toUpperCase();
        }
        String output = null;
        if (input != null) {
            char[] g = input.toCharArray();
            for (int y = 0; y < g.length; y++) {
                char subs = g[y];
                int pos = -1;
                pos = ScrubUtils.ALPHA_NUMERIC_LIST_UPPER_ALPHA.indexOf("" + subs);
                if (pos >= 0) {
                    subs = ScrubUtils.ALPHA_NUMERIC_LIST_UPPER_ALPHA_SHUFFLED.charAt(pos);
                } else {
                    pos = ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA.indexOf("" + subs);
                    if (pos >= 0) {
                        subs = ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA_SHUFFLED.charAt(pos);
                    } else {
                        pos = ScrubUtils.ALPHA_NUMERIC_LIST_NUMERIC.indexOf("" + subs);
                        if (pos >= 0) {
                            subs = ScrubUtils.ALPHA_NUMERIC_LIST_NUMERIC_SHUFFLED.charAt(pos);
                        }
                    }
                }
                g[y] = subs;
            }
            output = (new String(g));
        }
        return output;

    }

    public static String SelfSubstituteString(String input) {
        String NUMERIC_SHUFFLED = SeededShuffleString(ScrubUtils.ALPHA_NUMERIC_LIST_NUMERIC, input);
        String LOWER_ALPHA_SHUFFLED = SeededShuffleString(ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA, NUMERIC_SHUFFLED).toLowerCase();
        String UPPER_ALPHA_SHUFFLED = SeededShuffleString(ScrubUtils.ALPHA_NUMERIC_LIST_UPPER_ALPHA, LOWER_ALPHA_SHUFFLED).toUpperCase();
        String output = null;
        if (input != null) {
            char[] g = input.toCharArray();
            for (int y = 0; y < g.length; y++) {
                char subs = g[y];
                int pos = -1;
                pos = ScrubUtils.ALPHA_NUMERIC_LIST_UPPER_ALPHA.indexOf("" + subs);
                if (pos >= 0) {
                    subs = UPPER_ALPHA_SHUFFLED.charAt(pos);
                } else {
                    pos = ScrubUtils.ALPHA_NUMERIC_LIST_LOWER_ALPHA.indexOf("" + subs);
                    if (pos >= 0) {
                        subs = LOWER_ALPHA_SHUFFLED.charAt(pos);
                    } else {
                        pos = ScrubUtils.ALPHA_NUMERIC_LIST_NUMERIC.indexOf("" + subs);
                        if (pos >= 0) {
                            subs = NUMERIC_SHUFFLED.charAt(pos);
                        }
                    }
                }
                g[y] = subs;
            }
            output = (new String(g));
        }
        return output;

    }

    public static String ReferenceScrub(String input){
        char[] types = "09".toCharArray();
        String output = null;
        if (input != null) {
            String seedStr = ReverseStr(input.substring(0,Math.min(16, input.length())));
            Random random = getStrRandSeed(seedStr);
            char[] g = input.toCharArray();
            int lim = 10;
            for (int y = 0; y < g.length; y++) {
                if (y == 16) {
                    random = getStrRandSeed(ReverseStr(input));
                }
                if ((g[y] >= types[0]) && (g[y] <= types[1])){
                    g[y] = ("" + (random.nextInt(lim))).toCharArray()[0];
                }
            }
            output = (new String(g));
        }
        return output;
    }

    public static String ReverseStr(String in){
        String out = "";
        for (int i=in.length()-1; i >= 0; i--){
            out += in.charAt(i);
        }
        return out;
    }

    public static Integer ExtractIntFromStr(String src, Integer def){
        char[] types = "09".toCharArray();
        Integer ret = def;
        String resStr = "";
        char[] srcC = src.toCharArray();
        for (int i=0; i<srcC.length; i++){
            if ((srcC[i] >= types[0]) && (srcC[i] <= types[1])){
                resStr += srcC[i];
            }
        }
        if (resStr.length() > 0){
            try{
                ret = Integer.parseInt(resStr);
            } catch(Exception err){
                //unable to parse the integer value for whatever reason...  ?
            }
        }
        return (ret > def ? ret : def);
    }

    public static String RefScrub(String source){
//        Integer cycles = Math.max(ExtractIntFromStr(((source != null && source.length() >= 16)?source.substring(5,16):""), new Integer(1)) % 5, 1);
        Integer cycles = 1;
        String src = source;
        if (src != null) {
            while (cycles > 0) {
                cycles--;
                src = ReferenceScrub(src);
            }
        }
        return src;
    }

    /**
     * This string garbler will replace contents of the string in a consistent manner such that:
     * numeric characters are replace with psuedo-random numeric characters
     * lower-case letters will be replaces with psuedo-random lower-case letters
     * upper-case letters will be replaces with psuedo-random upper-case letters
     * all other characters remain the same.
     * <p/>
     * WARNING: if strings contain meaningful names containing characters outside of a-Z or 0-9... they might not be sufficiently garbled to hide the original meanings.
     * <p/>
     * <p/>
     * ...note to reader: this has been done in the simplest way I know and have absolutely no doubt that there are a billion optimisations that can be made to the code.
     */
    public static String SelfGarbleString(String input) {
        char[] types = "azAZ09".toCharArray();
        String output = null;
        if (input != null) {
            Random random = getStrRandSeed(input);
            char[] g = input.toCharArray();
            for (int y = 0; y < g.length; y++) {
                int lim = (((g[y] >= types[0] && g[y] <= types[1]) || (g[y] >= types[2] && g[y] <= types[3])) ? 26 : ((g[y] >= types[4] && g[y] <= types[5]) ? 10 : 0));
                int base = (((g[y] >= types[0] && g[y] <= types[1]) ? (int) types[0] : ((g[y] >= types[2] && g[y] <= types[3])) ? (int) types[2] : ((g[y] >= types[4] && g[y] <= types[5]) ? (int) types[4] : 0)));
                if (lim >= 26) {
                    g[y] = (char) (random.nextInt(lim) + base);
                } else if (lim >= 10) {
                    g[y] = ("" + (random.nextInt(lim))).toCharArray()[0];
                }
            }
            output = (new String(g));
        }
        return output;
    }

    public static String SeededGarbleString(String input, String seed) {
        char[] types = "azAZ09".toCharArray();
        String output = null;
        if (input != null) {
            Random random = getStrRandSeed(seed);
            Random g_rand = getStrRandSeed(globalSeed);
            char[] g = input.toCharArray();
            for (int y = 0; y < g.length; y++) {
                int lim = (((g[y] >= types[0] && g[y] <= types[1]) || (g[y] >= types[2] && g[y] <= types[3])) ? 26 : ((g[y] >= types[4] && g[y] <= types[5]) ? 10 : 0));
                int base = (((g[y] >= types[0] && g[y] <= types[1]) ? (int) types[0] : ((g[y] >= types[2] && g[y] <= types[3])) ? (int) types[2] : ((g[y] >= types[4] && g[y] <= types[5]) ? (int) types[4] : 0)));
                if (lim >= 26) {
                    int rchar = ((random.nextInt(lim) + g_rand.nextInt(lim)) % lim);
                    g[y] = (char) (rchar + base);
                } else if (lim >= 10) {
                    int rchar = ((random.nextInt(lim) + g_rand.nextInt(lim)) % lim);
                    g[y] = ("" + (random.nextInt(lim))).toCharArray()[0];
                }
            }
            output = (new String(g));
        }
        return output;
    }

    public static String FuzzyCaesarModulusString(String input) {
        char[] types = "azAZ09".toCharArray();
        String output = null;
        if (input != null) {
            Random random = getStrRandSeed(globalSeed);
            char[] g = input.toCharArray();
            for (int y = 0; y < g.length; y++) {
                int lim = (((g[y] >= types[0] && g[y] <= types[1]) || (g[y] >= types[2] && g[y] <= types[3])) ? 26 : ((g[y] >= types[4] && g[y] <= types[5]) ? 10 : 0));
                int base = (((g[y] >= types[0] && g[y] <= types[1]) ? (int) types[0] : ((g[y] >= types[2] && g[y] <= types[3])) ? (int) types[2] : ((g[y] >= types[4] && g[y] <= types[5]) ? (int) types[4] : 0)));
                if (lim == 26) {
                    int rchar = ((random.nextInt(lim) * ((int) g[y])) % lim);
                    g[y] = (char) (rchar + base);
                } else if (lim == 10) {
                    int rchar = ((random.nextInt(lim) * ((int) g[y])) % lim);
                    g[y] = ("" + (rchar)).toCharArray()[0];
                }
            }
            output = (new String(g));
        }
        return output;
    }


    public static String CaesarString(String input) {
        char[] types = "azAZ09".toCharArray();
        String output = null;
        if (input != null) {
            Random random = getStrRandSeed(globalSeed);
            char[] g = input.toCharArray();
            for (int y = 0; y < g.length; y++) {
                int lim = (((g[y] >= types[0] && g[y] <= types[1]) || (g[y] >= types[2] && g[y] <= types[3])) ? 26 : ((g[y] >= types[4] && g[y] <= types[5]) ? 10 : 0));
                int base = (((g[y] >= types[0] && g[y] <= types[1]) ? (int) types[0] : ((g[y] >= types[2] && g[y] <= types[3])) ? (int) types[2] : ((g[y] >= types[4] && g[y] <= types[5]) ? (int) types[4] : 0)));
                if (lim == 26) {
                    int rchar = ((random.nextInt(lim) + ((int) g[y])) % lim);
                    g[y] = (char) (rchar + base);
                } else if (lim == 10) {
                    int rchar = ((random.nextInt(lim) + ((int) g[y])) % lim);
                    g[y] = ("" + (rchar)).toCharArray()[0];
                }
            }
            output = (new String(g));
        }
        return output;
    }

    /**
     * This function will be used to do some rudimentary hashing shuffling and/garbling
     */
    public static String doTransform(String value, String action) {
        return doTransform(value, action, null, null);
    }

    public static String doTransform(String value, String action, HashMap<String, Integer> headers, String[] fields) {
        //set nulls to a nullstring value.
        if ("NULL".equalsIgnoreCase(value)) {
            value = "";
        }
        //PARSE INPUT PARAMS ETC
        //INJECT VALUES INTO THE INPUT VALUE OR PARAMS FROM OTHER RELATED COLUMNS...
        String[] params = action.split(" ");
        if ((headers != null) && (headers.size() > 0) && (fields != null) && (fields.length > 0)) {
            String[] headerKeys = headers.keySet().toArray(new String[0]);
            int fieldIndex = -1;
            for (int i = 0; i < params.length; i++) {
                for (int j = 0; j < headers.size(); j++) {
                    try {
                        fieldIndex = headers.get(headerKeys[j]);
                        params[i] = params[i].replaceAll("\\[" + headerKeys[j] + "\\]", fields[fieldIndex].replaceAll("\"", ""));
                    } catch (Exception err) {
                    }
                }
            }
            //IF THE VALUE HAS A FIELD REFERENCE, THEN POPULATE IT AS NECESSARY...

            for (int j = 0; j < headers.size(); j++) {
                try {
                    fieldIndex = headers.get(headerKeys[j]);
                    value = value.replaceAll("\\[" + headerKeys[j] + "\\]", fields[fieldIndex].replaceAll("\"", ""));
                } catch (Exception err) {
                }
            }

        }

        //PARSE ACTIONS...
        //now work with the data...
        if ((action == null) || (action.trim().length() == 0)) {
            return value;
        } else if ("GARBLE".equalsIgnoreCase(action)) {
            return SelfGarbleString(value);
        } else if ("OBF".equalsIgnoreCase(params[0]) && params.length > 1) {
            String garbd = CaesarString(params[1]);
            return garbd;
        } else if ("REF".equalsIgnoreCase(action)) {
            return RefScrub(value);
        } else if ("OBF".equalsIgnoreCase(action)) {
            return CaesarString(value);
        } else if ("SA_ID_DOB".equalsIgnoreCase(params[0]) && params.length >= 1) {
            return paramGen_SA_ID_FROM_DOB(params, action, value);
        } else if ("SA_ID".equalsIgnoreCase(action)) {
            if ((value != null) && (value.length() == 13)) {
                return GenSAID(value);
            }
            return value;
        } else if ("DATE".equalsIgnoreCase(action)) {
            if (value.length() >= 10) {
                return GenVarDate(StrToCal(value.substring(0,10).replaceAll("\\\\","-"), "yyyy-MM-dd"), 2);
            }
            return value;
        } else if ("DOB".equalsIgnoreCase(action)) {
            //DOB
            return paramDOBScrub(params, action, value);
        } else if ("DOB".equalsIgnoreCase(params[0]) && params.length > 1) {
            //DOB [RES_ID_NUMBER] <RandomOffsetDays>
            //DOB [RES_ID_NUMBER]
            //TODO: DOB <YYYYMMDD>
            //TODO: DOB <YYMMDD>
            //TODO: DOB <RandomOffsetDays>
            //DOB
            return paramDOBScrub(params, action, value);
        } else if ("SEEDED_GARBLE".equalsIgnoreCase(params[0]) && params.length >= 1) {
            return paramSeededGarble(params, action, value);
        } else if ("CONCAT".equalsIgnoreCase(params[0]) && params.length > 1) {
            return (((value != null) && (!"null".equalsIgnoreCase(value.trim().replaceAll("\"","").replaceAll("'",""))) && (value.length() > 0)) ? paramConcat(params, action, value) : value);
        } else if ("CONCAT".equalsIgnoreCase(params[0].substring(0,6)) && "(".equalsIgnoreCase(params[0].substring(6,7)) && params.length > 1) {
            return (((value != null) && (!"null".equalsIgnoreCase(value.trim().replaceAll("\"","").replaceAll("'",""))) && (value.length() > 0)) ? paramConcat(params, action, value) : value);
        } else if ("SHUFFLE".equalsIgnoreCase(action)) {
            return SelfShuffleString(value);
        } else if (action.toUpperCase().startsWith("ID")) {
            if (params.length == 2) {
                return GenSAID(params[1]);
            } else if (params.length == 3) {
                return GenSAID(parseInt(params[1], 0), params[2].equals("M"), true);
            }
            return (value.length() > 0 ? GenSAID() : "");
        }
        //NO VALUE, CHECK FOR FIELD REFERENCE AS A PARAM...
        return ((((value == null) || (value.length() == 0)) && (params.length == 1)) ? params[0] : value); //if the param was one of the above scrub transforms, then it should have returned by now.
    }

    private static String paramConcat(String[] params, String action, String value) {  //CONCAT [VALUE_1] " " [VALUE_2] ":" [VALUE_3] ... [VALUE_N]
        String returnVal = "";
        if ((value != null) && (!"null".equalsIgnoreCase(value.trim().replaceAll("\"","").replaceAll("'",""))) && (value.length() > 0)){
            //This is the only place to stick this for now (very kludgy).
            //...trim the data for this field to accommodate the prefix/suffix.
            int RLTrim = 0;
            int lp = params[0].indexOf("(");
            int rp = params[0].indexOf(")");
            int trimLength = 0;
            int lt = 0;
            int rt = 0;
            int trimBy = 0;
            int trimParam = 0;
            if ((lp > 0) && (rp > 0) && ((lp + 1) < (rp)) ) {
                String trimStr = params[0].substring(lp + 1,rp);
                if (trimStr.length() > 0) {
                    RLTrim = ((trimStr.toUpperCase().indexOf("L") >= 0)?-1:1);
                    trimStr = trimStr.toUpperCase().replaceAll("L","").replaceAll("R", "");
                    trimLength = parseInt(trimStr, 0);
                }

                int appendLength = 0;
                //now work out which param to trim and by how much...
                for (int i=1; i<params.length; i++){
                    if (params[i].equals(value)){
                        trimParam = i;
                    } else {
                        appendLength += CleanCharacters(params[i]).length();
                    }
                }
                if (((appendLength + CleanCharacters(params[trimParam]).length()) > trimLength) && (trimParam > 0)) {
                    trimBy = (appendLength + CleanCharacters(params[trimParam]).length()) - trimLength;
                    if (RLTrim > 0) {
                        lt = 0;
                        rt = trimLength - trimBy;
                    } else {
                        rt = trimLength;
                        lt = trimBy;
                    }
                }
            }

            for (int i=1; i<params.length; i++){
                String paramVal = CleanCharacters(params[i]);
                if ((trimParam == i) && (trimBy > 0)){
                    paramVal = paramVal.substring(lt, rt-1);
                }
                if (!"null".equalsIgnoreCase(paramVal)) {
                    returnVal += paramVal;
                }
            }
            return ((returnVal.length() > 0) && (value != null) && (!"null".equalsIgnoreCase(CleanCharacters(value))) && (value.length() > 0))? returnVal : value;
        }
        return value;
    }

    private static String CleanCharacters(String param) {
        return param != null?param.replaceAll("'", "").replaceAll("\"", "").replaceAll("\\n", " ").replaceAll("\\r", "").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r").replaceAll("\\\\t", "\t"):param;
    }

    public static String paramGen_SA_ID_FROM_DOB(String[] params, String action, String value) {
        if ((params.length > 1) && (params[1].length() > 0) && (!"NULL".equalsIgnoreCase(params[1].trim()))) {
//                params[1] = params[1].replaceAll("\\\\", "-");
            params[1] = params[1].length() >= 10 ? params[1].substring(2, 10) : params[1];
            params[1] = params[1].replaceAll("\\\\", "").replaceAll("-", "");
            int dob = parseInt(params[1]);
            if (params.length >= 4) {
//                return GenSAID(parseInt(params[1]), "m".equalsIgnoreCase(params[2]), params[3]);
                return GenSAID(dob, "m".equalsIgnoreCase(params[2]), true);
            } else if (params.length == 3) {
                return GenSAID(dob, "m".equalsIgnoreCase(params[2]), true);
            } else if (params.length == 2) {
                return GenSAID(dob, true, true);
            }
        } else if (params.length == 1) {
//            return GenSAID(value.substring(2, 10).replaceAll("\\\\", "-"));
            return GenSAID(value);
        }
        return value;
    }

    public static String paramSeededGarble(String[] params, String action, String value) {
        String garbd = "";
        if (params.length > 1) {
            int sbstrlen = params[1].length();
            String seed = params[1];
            try {
                int tmp = Math.min(parseInt(params[2], sbstrlen), sbstrlen);
                sbstrlen = tmp >= 0 ? tmp : sbstrlen;
            } catch (Exception err) {
                //this is likely because the second param is not supplied etc.
            }
            try {
                seed += params[1].substring(0, sbstrlen - 1);
            } catch (Exception Error) {

            }
            garbd = SeededGarbleString(params[1], seed);
            System.out.println(params[0] + ": " + params[1] + " (" + seed + ") --> " + garbd);
        } else {
            garbd = SeededGarbleString(value, ScrubUtils.globalSeed);
            System.out.println(params[0] + ": " + value + " (" + ScrubUtils.globalSeed + ") --> " + garbd);
        }
        return garbd;
    }

    public static String paramDOBScrub(String[] params, String action, String value) {
        Calendar dt = null;
        value = value!=null?value.replaceAll("'",""):null; //Strip out unwanted/useless chars
        int CurrentDecade = 17; //...as in 2017
        int OffsetRandomDays = 2;  //Variance of 2 days either way by default...
        try{
            CurrentDecade = Calendar.getInstance().get(Calendar.YEAR) % 100;
        } catch(Exception err){
            //Unable to get the current decade value.
        }
        //TODO: Clean-up the code below, refactor and generalise as needed - common code should be extracted and not repeated.
        if ((params.length > 1) && (params[1].length() > 0) &&
                (!"NULL".equalsIgnoreCase(params[1].trim())) &&
                (params[1].trim().matches("^\\d{13}$"))
                ) {
            String dtStr = params[1].substring(0,2)+"-"+params[1].substring(2,4)+"-"+params[1].substring(4,6);
            String dtPrefix = ((parseInt(params[1].substring(0,2),CurrentDecade) >= CurrentDecade) ? "19" : "20");
            dt = StrToCal(dtPrefix + dtStr, "yyyy-MM-dd");
        } else if ((params.length > 1) && (params[1].length() > 0) &&
                (!"NULL".equalsIgnoreCase(params[1].trim())) &&
                (params[1].trim().matches("^\\d{6}$"))
                ) {
            String dtStr = params[1].substring(0,2)+"-"+params[1].substring(2,4)+"-"+params[1].substring(4,6);
            String dtPrefix = ((parseInt(params[1].substring(0,2),CurrentDecade) >= CurrentDecade) ? "19" : "20");
            dt = StrToCal(dtPrefix + dtStr, "yyyy-MM-dd");
        } else if ((params.length > 1) && (params[1].length() > 0) &&
                (!"NULL".equalsIgnoreCase(params[1].trim())) &&
                (params[1].trim().matches("^\\d{8}$"))
                ) {
            String dtStr = params[1].substring(2,4)+"-"+params[1].substring(4,6)+"-"+params[1].substring(6,8);
            String dtPrefix = ((parseInt(params[1].substring(0,2),CurrentDecade) >= CurrentDecade) ? "19" : "20");
            dt = StrToCal(dtPrefix + dtStr, "yyyy-MM-dd");
        } else if ((params.length > 1) && (params[1].length() > 0) &&
                (!"NULL".equalsIgnoreCase(params[1].trim())) &&
                (params[1].trim().matches("^\\d{4}[-\\\\/]\\d{2}[-\\\\/]\\d{2}$"))
                ) {
            String paramStr = params[1].replaceAll("\\\\","").replaceAll("/", "").replaceAll("-","").trim();
            String dtStr = paramStr.substring(0, 2)+"-"+paramStr.substring(2, 4)+"-"+paramStr.substring(4, 6);
            String dtPrefix = ((parseInt(paramStr.substring(0, 2),CurrentDecade) >= CurrentDecade) ? "19" : "20");
            dt = StrToCal(dtPrefix + dtStr, "yyyy-MM-dd");
        } else if ((params.length == 1) && (value != null) &&
                (!"NULL".equalsIgnoreCase(value.trim())) &&
                (value.length() >= 6) &&
                (value.replaceAll("\\\\","").replaceAll("/", "").replaceAll("-","").matches("^\\d{6,8}.*?$"))
                ){
            String dtStr = "";
            String valStr = value.replaceAll("\\\\","").replaceAll("/", "").replaceAll("-","");
            if (value.length() >= 8) {
                dtStr = valStr.substring(2,4) + "-" + valStr.substring(4, 6) + "-" + valStr.substring(6, 8);
            } else if (value.length() == 6) {
                dtStr = valStr.substring(0,2) + "-" + valStr.substring(2,4) + "-" + valStr.substring(4, 6);
            }
            String dtPrefix = ((parseInt(dtStr.substring(0,2),CurrentDecade) >= CurrentDecade) ? "19" : "20");
            dt = StrToCal(dtPrefix + dtStr, "yyyy-MM-dd");
        }

        if (params.length == 3) {
            OffsetRandomDays = parseInt(params[2], OffsetRandomDays);
        } else if ((params.length == 2) &&
                (params[1] != null) &&
                (params[1].trim().matches("^\\d{1,4}$"))
                ){
            OffsetRandomDays = parseInt(params[1], OffsetRandomDays);
        }

        if (dt != null) {
            return GenVarDate(dt, OffsetRandomDays);
        } else if ((value!=null) && (value.length() >= 10)) {
            String dtPrefix = ((parseInt(value.substring(2,4),CurrentDecade) >= CurrentDecade) ? "19" : "20");
            return GenVarDate(StrToCal(dtPrefix+value.substring(2,10).replaceAll("\\\\","-").replaceAll("/","-"), "yyyy-MM-dd"), OffsetRandomDays);
        }
        return value;
    }

    public static String quote(String value, boolean quote) {
        //either do not quote already quoted strings OR we need to escape the quotes...
        value = value.trim();
        if (quote) {
            if (value.length() > 1) {
                if (value.charAt(0) != '"' && value.charAt(value.length() - 1) != '"') {
                    //cop out and simply do not quote already quoted strings...
                    quote = false;
                    //value = value.trim(); //...do we really want to?
                } else if (value.charAt(0) == '"' || value.charAt(value.length() - 1) == '"') {
                    //the string is not fully quoted - replace the existing quotes with a place holder...
                    value = value.replaceAll("\"", QUOTE_SUBS);
                }
            }
        }
        return (quote ? "\"" : "") + value + (quote ? "\"" : "");
    }
}
