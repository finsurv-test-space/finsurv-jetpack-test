package examples;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.utils.FreemarkerUtil;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;
import za.co.synthesis.javascript.JSObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FreemarkerTemplatesTests {
    private static final Logger logger = LoggerFactory.getLogger(FreemarkerTemplatesTests.class);
    public static String channelName = "sbZA";
    public static String localArtefactDirectory = "./producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "http://localhost:8084/report-data-store/";
    public static String reportDataStoreUsername = "superuser";
    public static String reportDataStorePassword = "abc";
    public static String finsurvReportTemplateDirectory = "./Templates";
    public static String rootFinsurvReportTemplate = "RootTemplate.ftl";
    /*
     Data that can be provided to the templating engine is as follows:
     1) Accounting information (account info, flow currency, domestic value, foreign value, value date...)
     2) ~static BOP info ... (read-only, ADLA indicator, channelname, .... )
     3) Debit Side info (BIC Code, account type, account holder, address info, country, currency, demographics...)
     4) Credit Side info (...)

     logic to be applied:
     from the decision, extract useful results such as:
     1) Flow
     2) Reporting Qualifier
     3) Side to use for Resident and/or Resident Type
     4) Side to use for NonResident and/or NonResident Type
     5) Account Types for Res and Non Res
     6) Rulings Section, *Bop Codes*
     */


    /**
     * The following tests are created to demonstrate how a BOP report can be generated from evaluation decisions, the DR detail, CR detail and accounting detail.
     * Freemarker is used to build up the BOP report based on logic that we have built-in in each of the templates.
     * The RootTemplate is the main template that's composed. This template calls the other sub-templates and the templates as a whole will build up the report structure that we know.
     * An example of the logic that has been built into the freemarker templates is a check to see if the data contains an entity name that would then load up the
     * - entity template instead of the individual or exception template.
     */

    /**
     * This scenario consists of a resident and a non resident with only one decision.
     */

    @Test
    public void testFreemarkerTemplating(){
        //Create an instance of finsurv local with the correct credentials and directories (for the artefacts and templates)
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);
        finsurv.addChannel(channelName);

        //Create object to store the results in
        List<String> result = new ArrayList<>();

        //Create object to store the sample data in
        HashMap<String, Object> params = new HashMap<String, Object>();
        //TODO: load the "SampleAccountingEntryDetail.json" (detailed scenario mapping) and use it to perform an evaluation.
        JSObject accDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/Basic/SampleAccountingEntryDetail.json");
        params.putAll(accDetail);

        //TODO: from the evaluation decision, above-mentioned "SampleAccountingEntryDetail.json" and the additional "SampleDRDemographic.json", and "SampleCRDemographic.json", generate a "Skeleton BOP Report" using the templates...
        JSObject evalDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/Basic/EvaluationReportableDecision.json");
        JSObject drDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/Basic/SampleDRDemographicDual.json");
        JSObject crDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/Basic/SampleCRDemographicDual.json");

        // Load in the sample data (account entry detail, evaluation decision detail, cr detail and dr detail).
        params.put("Evaluation", evalDetail);
        params.put("cr", crDetail);
        params.put("dr", drDetail);
        try {
            //Generate the bop reports with the data provided by using the freemarker templates
            result = finsurv.generateBopRecords(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("\n\n\n"+result+"\n\n\n");
    }

    /**
     *  This scenario should generate two reports - one for DR and one for CR - both should be reportable
     */
    @Test
    public void testFreemarkerTemplatingCfcToCfc(){
        //Create an instance of finsurv local with the correct credentials and directories (for the artefacts and templates)
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);
        finsurv.addChannel(channelName);

        //Create object to store the results in
        List<String> result = new ArrayList<>();
        //Create object to store the sample data in
        HashMap<String, Object> params = new HashMap<String, Object>();

        // Load in the sample data (account entry detail, evaluation decision detail, cr detail and dr detail).
        JSObject accDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/CfcToCfc/SampleAccountingEntryDetail.json");
        params.putAll(accDetail);

        JSObject evalDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/CfcToCfc/EvaluationReportableDualDecision.json");
        JSObject drDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/CfcToCfc/SampleDRDemographicDual.json");
        JSObject crDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/CfcToCfc/SampleCRDemographicDual.json");

        params.put("Evaluation", evalDetail);
        params.put("cr", crDetail);
        params.put("dr", drDetail);
        try {
            //Generate the bop reports with the data provided by using the freemarker templates
            result = finsurv.generateBopRecords(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("\n\n\n"+result+"\n\n\n");
    }

    /**
     * This scenario consists of two non residents with only one decision
     */
    @Test
    public void testFreemarkerTemplatingForResException(){
        String result = "ERM...";
        HashMap<String, Object> params = new HashMap<String, Object>();
        //TODO: load the "SampleAccountingEntryDetail.json" (detailed scenario mapping) and use it to perform an evaluation.
        JSObject accDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/ResException/SampleAccountingEntryDetail.json");
        params.putAll(accDetail);

        //TODO: from the evaluation decision, above-mentioned "SampleAccountingEntryDetail.json" and the additional "SampleDRDemographic.json", and "SampleCRDemographic.json", generate a "Skeleton BOP Report" using the templates...
        JSObject evalDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/ResException/EvaluationReportableOutDecisionResException.json");
        JSObject drDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/ResException/SampleDRDemographic.json");
        JSObject crDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/ResException/SampleCRDemographic.json");

        params.put("eval", evalDetail);
        params.put("cr", crDetail);
        params.put("dr", drDetail);
        try {
            result = FreemarkerUtil.composeTemplateFile("./Templates", "RootTemplate.ftl", params, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("\n\n\n"+result+"\n\n\n");
    }

    /**
     * This scenario consists of two residents with multiple decisions
     */
    @Test
    public void testFreemarkerTemplatingForNonResException(){
        String result = "ERM...";
        HashMap<String, Object> params = new HashMap<String, Object>();
        //TODO: load the "SampleAccountingEntryDetail.json" (detailed scenario mapping) and use it to perform an evaluation.
        JSObject accDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/NonResException/SampleAccountingEntryDetail.json");
        params.putAll(accDetail);

        //TODO: from the evaluation decision, above-mentioned "SampleAccountingEntryDetail.json" and the additional "SampleDRDemographic.json", and "SampleCRDemographic.json", generate a "Skeleton BOP Report" using the templates...
        JSObject evalDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/NonResException/EvaluationReportableOutDecisionNonResException.json");
        JSObject drDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/NonResException/SampleDRDemographic.json");
        JSObject crDetail = (JSObject) JsonUtils.loadJsonFile("./sample/SampleDataForTemplates/NonResException/SampleCRDemographic.json");

        params.put("eval", evalDetail);
        params.put("cr", crDetail);
        params.put("dr", drDetail);
        try {
            result = FreemarkerUtil.composeTemplateFile("./Templates", "RootTemplate.ftl", params, null);
            result.replaceAll("^\\s*$", "");

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("\n\n\n"+result+"\n\n\n");
    }
}
