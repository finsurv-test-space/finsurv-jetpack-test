package examples;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.utils.HistoryUtils;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions.getFileContent;
import static za.co.synthesis.finsurvlocal.utils.JsonUtils.listToJsonStr;

public class HistoryExample {
    private static final Logger logger = LoggerFactory.getLogger(HistoryExample.class);
    public static String channelName = "sbZA";
    public static String localArtefactDirectory = "./producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "http://localhost:8084/report-data-store/";
    public static String reportDataStoreUsername = "superuser";
    public static String reportDataStorePassword = "abc";
    public static String finsurvReportTemplateDirectory = "./Templates";
    public static String rootFinsurvReportTemplate = "RootTemplate.ftl";


    @Test
    public void historyExampleComparesJsonMapsAndReturnsString() throws Exception {

        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();
        String jsonStr1 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON.json");
        String jsonStr2 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON2.json");
        Map<String, Object> jsonMap1 = JsonUtils.jsonStrToMap(jsonStr1);
        Map<String,  Object> jsonMap2 = JsonUtils.jsonStrToMap(jsonStr2);
        String out = "";
        String diffResult;

        try {
            diffResult = finsurv.getReportDiffString(jsonMap1, jsonMap2);
            out += diffResult;
        } catch (Exception error) {
            //oh dang-it
        }
        logger.info("\n\n diffResult = " + out);
        logger.info("\n\n\nFinished processing.");
    }

    @Test
    public void historyExampleComparesJsonMapsAndReturnsMap() throws Exception {

        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();
        String jsonStr1 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON.json");
        String jsonStr2 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON2.json");
        Map<String, Object> jsonMap1 = JsonUtils.jsonStrToMap(jsonStr1);
        Map<String, Object> jsonMap2 = JsonUtils.jsonStrToMap(jsonStr2);
        String out = "";
        Map<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<String>>> diffResult;
        try {
            diffResult = finsurv.getReportDifferenceMap(jsonMap1, jsonMap2);
            for (Map.Entry<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<String>>> entry : diffResult.entrySet()) {
                out += "SECTION: " + entry.getKey().toString() + "\r\n";
                for (Map.Entry<HistoryUtils.DiffActionType, List<String>> entry2 : entry.getValue().entrySet()) {
                    out += "\t-\t" + entry2.getKey().toString() + ":\t" + listToJsonStr(entry2.getValue()) + "\r\n";
                }
            }
        } catch (Exception error) {
            //oh dang-it
        }
        logger.info("\n\n diffResult = " + out);
        logger.info("\n\n\nFinished processing.");
    }

    @Test
    public void historyExampleComparesJsonStringsAndReturnsMap() throws Exception {

        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();
        String jsonStr1 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON.json");
        String jsonStr2 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON2.json");
        String out = "";
        Map<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<String>>> diffResult;
        try {
            diffResult = finsurv.getReportDifferenceMap(jsonStr1, jsonStr2);
            for (Map.Entry<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<String>>> entry : diffResult.entrySet()) {
                out += "SECTION: " + entry.getKey().toString() + "\r\n";
                for (Map.Entry<HistoryUtils.DiffActionType, List<String>> entry2 : entry.getValue().entrySet()) {
                    out += "\t-\t" + entry2.getKey().toString() + ":\t" + listToJsonStr(entry2.getValue()) + "\r\n";
                }
            }
        } catch (Exception error) {
            //oh dang-it
        }
        logger.info("\n\n diffResult = " + out);
        logger.info("\n\n\nFinished processing.");
    }

    @Test
    public void historyExampleComparesJsonStringsAndReturnsString() {

        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();
        String jsonStr1 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON.json");
        String jsonStr2 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON2.json");

        String out = "";
        out += "\n\n####################################################\n\n" +
                finsurv.getReportDiffString(jsonStr1, jsonStr2) +
                "\n\n####################################################\n\n";
        out += "\n\n\nFinished processing.";
        System.out.println(out);
    }

    @Test
    public void historyExampleComparesJsonStringsAndReturnsObject() {

        //-----------------------------------------------------------------------------------
        //  BOOTSTRAP CODE
        //-----------------------------------------------------------------------------------

        //Create an instance of Finsurv Local with the correct artefact directory, RDS instance url, credentials, report directory and root template name.
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);

        //This is just to generate unique records
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>();
        String jsonStr1 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON.json");
        String jsonStr2 = getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON2.json");

        String out = "";
        Map<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<Object>>> diffResult;
        try {
            diffResult = finsurv.getReportDiff(jsonStr1, jsonStr2);
            for (Map.Entry<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<Object>>> entry : diffResult.entrySet()) {
                out += "SECTION: " + entry.getKey().toString() + "\r\n";
                for (Map.Entry<HistoryUtils.DiffActionType, List<Object>> entry2 : entry.getValue().entrySet()) {
                    out += "\r\n\r\n\t-\t#####  " + entry2.getKey().toString() + "  #####:\t" + listToJsonStr(entry2.getValue()) + "\r\n";
                }
            }
        } catch (Exception error) {
            //oh dang-it
        }
        out = "\n\n####################################################\n\n" +
                out +
                "\n\n####################################################\n\n";
        out += "\n\n\nFinished processing.";
        System.out.println(out);
    }
}
