package examples;

import org.junit.Test;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSFunctionCall;
import za.co.synthesis.javascript.JSScope;
import za.co.synthesis.javascript.JSStructureParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions.*;

public class DocumentValidationTests {
  public static String channelName = "sbZA";
  public static String localArtefactDirectory = "./producer/api/rules/";
  public static String reportDataStoreInstanceUrl = "http://localhost:81/report-data-store/";
  public static String reportDataStoreUsername = "";
  public static String reportDataStorePassword = "";
  public static FinsurvLocal finsurvLocal;

  public static String finsurvReportTemplateDirectory = null;
  public static String rootFinsurvReportTemplate = null;

  public DocumentValidationTests(){
    finsurvLocal = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword,finsurvReportTemplateDirectory,rootFinsurvReportTemplate);
    finsurvLocal.addChannel(channelName);
    finsurvLocal.updateAllChannelsArtefacts();
  }


  @Test
  public void testDocumentValidation() throws InterruptedException {
    Thread.sleep(1);
    try {
      //check for document requirements based on the data provided...
      List<String> reqResults = finsurvLocal.getRequiredDocs(getFileContent("./sample/ExampleBopData/ExampleFinsurvJSON.json"), "sbZA");
      for (String doc : reqResults){
        System.out.println(doc);
      }
    } catch (Exception e){
      //
    }
    try {
      //check for the potentially required documents based on the flow and category code...
      List<String> potResults = finsurvLocal.getPotentialDocs("sbZA", "101/01", "IN");
      for (String doc : potResults) {
        System.out.println(doc);
      }
    } catch (Exception e){
      //
    }
  }


  @Test
  public void GetAllCategoriesWithNoDocUploads() throws Exception{
    //Allow the retrieval of artefacts to complete
    Thread.sleep(500);

    int cnt = 0;
    try {
      HashMap<String, List<String>> potentialDocs = new HashMap<>();
      String channelName = "sbZA";
      String lookups = getArtefactLookups(channelName);
      JSStructureParser structureParser = new JSStructureParser(lookups);
      za.co.synthesis.javascript.JSObject lookupsJson = (za.co.synthesis.javascript.JSObject) structureParser.parse();
      //rough-n-ready extraction of lookups data from the lookups file...
      if (lookupsJson instanceof JSScope && lookupsJson.containsKey("define")){
        JSArray params = ((JSFunctionCall) ((JSScope) lookupsJson).get("define")).getParameters();
        if (params.size() == 1 && params.get(0) instanceof Map && ((Map<String,Object>)params.get(0)).containsKey("lookups")){
          lookupsJson = (za.co.synthesis.javascript.JSObject) ((Map<String,Object>)params.get(0)).get("lookups");
        }
      }
      //extracting category codes from lookups...
      if (lookupsJson != null && lookupsJson.containsKey("categories") && lookupsJson.get("categories") instanceof List) {
        List<Map<String, Object>> categories = (List<Map<String, Object>>) lookupsJson.get("categories");
        for (Map<String, Object> categoryObj : categories) {
          cnt++;
          List<String> potResults = finsurvLocal.getPotentialDocs(channelName, (String) categoryObj.get("code"), (String) categoryObj.get("flow"));
          if (potResults == null || potResults.size() == 0) {
            System.out.println("No docs configured for channel=" + channelName +
                    ", Flow=" + ((String)categoryObj.get("flow")) +
                    ", Category=" + ((String)categoryObj.get("code")) + " (" + ((String)categoryObj.get("section")) + " - " + ((String)categoryObj.get("description")) + ")");
          }
        }
      }
    } catch (Exception e){
      //
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
    System.out.println("Total number of potential document scenarios tested: "+cnt);
  }


}
