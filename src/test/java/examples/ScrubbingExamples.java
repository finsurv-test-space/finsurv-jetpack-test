package examples;

import org.junit.Test;
import util.ScrubUtils;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;


/**
 * This is an example of submission and retrieval of a single report to/from RDS
 */
public class ScrubbingExamples {
    public static String channelName = "sbZA";
    public static String localArtefactDirectory = "./producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "http://localhost:81/report-data-store/";
    public static String reportDataStoreUsername = "superuser";
    public static String reportDataStorePassword = "abc";
    public static String finsurvReportTemplateDirectory = null;
    public static String rootFinsurvReportTemplate = null;


    public static String Seed = "p988h3224f498hfweeh893922q";
    public static ScrubUtils scrubUtil = new ScrubUtils();


    public static void resetScrubber(){
        scrubUtil.globalSeed = scrubUtil.GlobalSubstituteString(scrubUtil.globalSeed);
        scrubUtil.ALPHA_NUMERIC_LIST_LOWER_ALPHA_SHUFFLED = "";
    }

    public static String scrubSubs(boolean scrub, String val) {
        scrubUtil.globalSeed = Seed;
        return (scrub ? scrubUtil.GlobalSubstituteString(val) : val);
    }

    public static String scrubGarb(boolean scrub, String val) {
        scrubUtil.globalSeed = Seed;
        return (scrub ? scrubUtil.SelfGarbleString(val) : val);
    }

    public static String seedGarb(boolean scrub, String val) {
        scrubUtil.globalSeed = Seed;
        return (scrub ? scrubUtil.SeededGarbleString(val, Seed) : val);
    }

    public static String scrubID(boolean scrub, String val) {
        scrubUtil.globalSeed = Seed;
        return (scrub ? (val == null || val.length() == 0 ? val : scrubUtil.GenSAID(val)) : val);
    }


    public static void testrun() {
        String testStr = "Denieke";
        System.out.println("Seed: "+Seed);
        System.out.println("--------");
        System.out.println("scrubSubs("+testStr+") => "+scrubSubs(true, testStr));
        System.out.println("scrubSubs("+testStr+") => "+scrubSubs(true, testStr));
        Seed = scrubSubs(true, Seed);
        System.out.println("Seed: "+Seed);
        System.out.println("scrubSubs("+testStr+") => "+scrubSubs(true, testStr));
        System.out.println("--------");
        System.out.println("scrubGarb("+testStr+") => "+scrubGarb(true, testStr));
        System.out.println("scrubGarb("+testStr+") => "+scrubGarb(true, testStr));
        Seed = scrubSubs(true, Seed);
        System.out.println("Seed: "+Seed);
        System.out.println("scrubGarb("+testStr+") => "+scrubGarb(true, testStr));
        System.out.println("--------");
        System.out.println("seedGarb("+testStr+") => "+seedGarb(true, testStr));
        System.out.println("seedGarb("+testStr+") => "+seedGarb(true, testStr));
        Seed = scrubSubs(true, Seed);
        System.out.println("Seed: "+Seed);
        System.out.println("seedGarb("+testStr+") => "+seedGarb(true, testStr));
        System.out.println("--------");
        testStr = "8008045153082";
        System.out.println("scrubID("+testStr+") => "+scrubID(true, testStr));
        System.out.println("scrubID("+testStr+") => "+scrubID(true, testStr));
        Seed = scrubSubs(true, Seed);
        System.out.println("Seed: "+Seed);
        System.out.println("scrubID("+testStr+") => "+scrubID(true, testStr));
        System.out.println("--------");
    }

    @Test
    public void runScrubberTests(){
        testrun();
        resetScrubber();
        testrun();
    }
}
