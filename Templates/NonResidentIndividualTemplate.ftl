  {
    "Individual": {
        "Name":
            <#if nres.Name??>
                "${nres.Name!''}"
            <#else>
                {}
            </#if>,
        "Surname":
            <#if nres.Surname??>
                "${nres.Surname!''}"
            <#else>
                {}
            </#if>,
        "Gender":
            <#if nres.Gender??>
                "${nres.Gender!''}"
            <#else>
                {}
            </#if>,
        "AccountIdentifier":
            <#if nres.AccountIdentifier??>
                "${nres.AccountIdentifier!''}"
            <#else>
                {}
            </#if>,
        "AccountName":
            <#if nres.AccountName??>
                "${nres.AccountName!''}"
            <#else>
                {}
            </#if>,
        "AccountNumber":
            <#if nres.AccountNumber??>
                "${nres.AccountNumber!''}"
            <#else>
                {}
            </#if>,

        "PassportCountry":
            <#if nres.PassportCountry??>
                "${nres.PassportCountry!''}"
            <#else>
                {}
            </#if>,
        "PassportNumber":
            <#if nres.PassportNumber??>
                "${nres.PassportNumber!''}"
            <#else>
                {}
            </#if>,

        "Address":
             <#if nres??>
                  <#if nres.Address??>
                    <#assign optTemp = .get_optional_template('NRAddressTemplate.ftl')>
                    <#if optTemp.exists>
                      <@optTemp.include />
                    <#else>
                        {}
                    </#if>
                  <#else>
                       <#if nres.StreetAddress??>
                             <#assign optTemp = .get_optional_template('NRStreetAddressTemplate.ftl')>
                             <#if optTemp.exists>
                               <@optTemp.include />
                             <#else>
                                {}
                             </#if>
                       <#else>
                          {}
                       </#if>
                  </#if>
             <#else>
                 {}
             </#if>
    }
  }
        